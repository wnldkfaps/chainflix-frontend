import Vue from 'vue'
import VueRouter from "vue-router"
import Landing from "../pages/landing.vue"
import NotFound from '../pages/notfound.vue'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: Landing },

        { path: '*', component: NotFound }
    ],
    // 페이지 전환시 화면 맨위로 스크롤 이동
    scrollBehavior () {
        return { x: 0, y: 0 }
    }
})

export default router
